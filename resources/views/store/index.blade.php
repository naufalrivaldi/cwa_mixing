@extends('master')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Data Cabang</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Cabang</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <a href="{{route('store.form')}}" class="btn btn-success">Tambah Cabang</a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Inisial</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>            
                @foreach($stores as $store)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $store->name }}</td>
                    <td>{{ $store->initial }}</td>
                    <td>
                      <a href="{{ route('store.edit', ['id'=>$store->id]) }}" class="btn btn-sm btn-warning fas fa-pencil-alt"></a>
                      <button class="btn btn-sm btn-danger far fa-trash-alt delete" data-id="{{ $store->id }}"></button>
                    </td>
                  </tr>    
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection

@section('js')

    <script>
      $(document).on('click','.delete',function() {     
          var id = $(this).data('id');
          // console.log(id);
          Swal.fire({
          title: 'Perhatian!',
          text: "Apakah anda yakin menghapus data ini?",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
          }).then((result) => {
            if (result.value) {
              $.ajax({
                type: "POST",
                url: "{{ route('store.delete') }}",
                data: {
                  id: id,
                  _token: '{{ csrf_token() }}'
                },
                success: function(data){
                  location.reload()
                }
              })
            }
          })
      });
    </script>

@endsection