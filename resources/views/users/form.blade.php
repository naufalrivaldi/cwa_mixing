@extends('master')

@section('content')
<!-- Page Header -->
<?php
  $id = '';
  if ($_GET) {
    $id = $_GET['id'];
  }
?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">{{($id)?'Edit Data Pengguna':'Input Data Pengguna'}}</h2>
            <div class="page-breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <a href="{{route('users')}}"><li class="breadcrumb-item" aria-current="page">Pengguna</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<!-- content -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-header">Form Pengguna</h3>
            <div class="card-body">
              <div class="container">
                <div class="row justify-content-md-center">
                  <div class="col-md-6">
                    <form action="{{ ($id)?route('users.update') : route('users.add') }}" method="post">
                    {{csrf_field()}}
                    @if($id)
                      <input type="hidden" value="{{$user->id}}" name="id">
                    @endif
                      <div class="form-group">
                        <label for="inputNama" class="col-form-label">Nama</label>
                        <input id="inputNama" type="text" class="form-control" name="name" value="{{($id)?$user->name:''}}">                        
                        @if($errors->has('name'))
                          <div class="text-danger">
                              {{ $errors->first('name') }}
                          </div>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="inputUsername" class="col-form-label">Email</label>
                        <input id="inputUsername" type="email" class="form-control" name="username" value="{{($id)?$user->username:''}}">
                        @if($errors->has('username'))
                          <div class="text-danger">
                              {{ $errors->first('username') }}
                          </div>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="inputPassword" class="col-form-label">Password</label>
                        @if($id)
                        <input id="inputPassword" type="hidden" class="form-control" name="passwordOld" value="{{($id?$user->password:'')}}">                        
                        @endif
                        <input id="inputPassword" type="password" class="form-control" name="password" value="">
                        @if($id)
                        <p class="text-danger">Kosongkan jika tidak ingin merubah password!</p>
                        @endif
                        @if($errors->has('password'))
                          <div class="text-danger">
                              {{ $errors->first('password') }}
                          </div>
                        @endif
                      </div> 
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Cabang</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="storeId" value="">
                        <option value=""></option>
                        @foreach($stores as $store)                        
                        <option value="{{$store->id}}" {{ ($id)?($user->storeId == $store->id) ? 'selected' : '' : '' }}>{{$store->name}}</option>
                        @endforeach 
                        </select>
                        @if($errors->has('storeId'))
                          <div class="text-danger">
                              {{ $errors->first('storeId') }}
                          </div>
                        @endif
                      </div>
                      <input type="submit" class="btn btn-primary" value="{{($id)?'Simpan':'Tambah'}}">
                      @if(!$id)
                      <input type="reset" class="btn btn-danger" value="Reset">
                      @endif
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection