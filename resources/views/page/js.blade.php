<!-- jquery 3.3.1 -->
<script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<!-- bootstap bundle js -->
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<!-- slimscroll js -->
<script src="{{ asset('vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<!-- main js -->
<script src="{{ asset('libs/js/main-js.js') }}"></script>
<!-- chart chartist js -->
<script src="{{ asset('vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
<!-- sparkline js -->
<script src="{{ asset('vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
<!-- chart c3 js -->
<script src="{{ asset('vendor/charts/c3charts/c3.min.js') }}"></script>
<script src="{{ asset('vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
<script src="{{ asset('vendor/charts/c3charts/C3chartjs.js') }}"></script>
<!-- datatable -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<!-- sweetalert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    $(document).ready( function () {
        $('.table').DataTable();
    } );
</script>
<script>      
    $(document).ready(function() {
        $('.confirmPassword').on('keyup', function() {
            var newPass = $('.newPassword').val();
            var confirmPass = $('.confirmPassword').val();            
            if (newPass != confirmPass) {
            $('.btnSave').attr("disabled",'disabled');
            $('.confirmPassword').addClass('is-invalid');            
            $('.confirmPassword').removeClass('is-valid');
            }else{              
            $('.confirmPassword').removeClass('is-invalid');
            $('.confirmPassword').addClass('is-valid');
            $('.btnSave').removeAttr('disabled');
            }
            console.log(confirmPass);
        
        })
    })
</script>

@yield('js');
