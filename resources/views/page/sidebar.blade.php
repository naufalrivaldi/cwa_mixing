<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="{{route('dashboard')}}">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    @if(auth()->user()->roles == '1')                    
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('dashboard') }}"><i class="fa fa-fw fa-home"></i>Dashboard </a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('store') }}"><i class="fa fa-fw fa-warehouse"></i>Cabang </a>
                    </li>
                    
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('users') }}"><i class="fa fa-fw fa-user-circle"></i>Pengguna </a>
                    </li>
                    @endif
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('customers') }}"><i class="fa fa-fw fa-users"></i>Pelanggan </a>
                    </li>                    
                    @if(auth()->user()->roles == '1')

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('merk') }}"><i class="fa fa-fw fas fa-dolly"></i>Mesin </a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('product') }}"><i class="fa fa-fw fas fa-dolly"></i>Produk </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('formula') }}"><i class="fas fa-flask"></i> Formula</a>
                    </li>
                    @endif
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('mixing') }}"><i class="fa fa-fw fas fa-paint-brush"></i>Mixing </a>
                    </li>
                    <!-- <li class="nav-item ">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4"><i class="fab fa-fw fa-wpforms"></i>Kuisioner</a>
                        <div id="submenu-4" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="backend/pertanyaan.php">Pertanyaan</a>
                                </li>
                            </ul>
                        </div>
                    </li> -->
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-file"></i>Laporan</a>
                        <div id="submenu-5" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="backend/penilaian.php">Penilaian</a>
                                </li>
                            </ul>
                        </div>
                    </li> -->
                </ul>
            </div>
        </nav>
    </div>
</div>