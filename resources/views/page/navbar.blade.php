<div class="dashboard-header">
    <nav class="navbar navbar-expand-lg bg-white fixed-top">
        <a class="navbar-brand" href="{{route('dashboard')}}">CWJA MIXING</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto navbar-right-top">
                <li class="nav-item dropdown nav-user">
                    <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Selamat datang, {{auth()->user()->name}} <i class="fas fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                        <a class="dropdown-item" data-toggle="modal" href="#changePassword"><i class="fas fa-lock mr-2"></i>Ubah Password</a>
                        <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-power-off mr-2"></i>Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Ubah Password {{auth()->user()->name}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">     
                <form action="{{route('changePassword')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="oldPassword">Password Lama</label>
                    <input type="password" class="form-control" id="oldPassword" name="oldpassword" aria-describedby="emailHelp">
                    @if($errors->has('oldpassword'))                        
                        <div class="text-danger">
                            {{ $errors->first('oldpassword') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="newPassword">Password Baru</label>
                    <input type="password" class="form-control newPassword" name="newpassword" aria-describedby="emailHelp">
                    @if($errors->has('newpassword'))                        
                        <div class="text-danger">
                            {{ $errors->first('newpassword') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="confirm">Konfirmasi Password</label>
                    <input type="password" class="form-control confirmPassword" name="newpassword2" aria-describedby="emailHelp">
                    @if($errors->has('newpassword2'))                        
                        <div class="text-danger">
                            {{ $errors->first('newpassword2') }}
                        </div>
                    @endif
                    <div class="valid-feedback">
                        Password sudah sesuai.
                    </div>
                    <div class="invalid-feedback">
                        Password tidak sesuai.
                    </div>
                </div>                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btnSave" disabled>Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>