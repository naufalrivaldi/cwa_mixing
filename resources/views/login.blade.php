<!doctype html>
<html lang="en">
 
<head>
    @include('page.header')
</head>

<body>
  <div class="splash-container mt-5">
    <div class="card ">
      <div class="card-header text-center">
        <img class="logo-img" width="50%" src="{{ asset('images/logocwa.png')}}" alt="logo"></a>
          <h2 class="mt-5">CWA Mixing</h2>
      </div>
      <div class="card-body">
        @include('page.alert')
        <form method="post" action="{{route('login.post')}}">
          {{csrf_field()}}
          <div class="form-group">
            <input class="form-control form-control-lg" name="username" id="username" type="text" placeholder="Username" autocomplete="off">
          </div>
          <div class="form-group">
            <input class="form-control form-control-lg" name="password" id="password" type="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
        </form>
      </div>
    </div>
  </div>
    @include('page.js')
</body>
 
</html>