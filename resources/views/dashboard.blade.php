@extends('master')

@section('content')
<!-- Page Header -->
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="page-header">
            <h2 class="pageheader-title">Dashboard </h2>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
        <a href="{{route('customers')}}">
        <div class="card">
            <div class="card-body">
                <h5 class="text-muted">Pelanggan</h5>
                <div class="metric-value d-inline-block">
                    <h1 class="mb-1">{{$customers}}</h1>
                </div>
                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                </div>
            </div>
            <div id="sparkline-revenue"></div>
        </div>
    </div>
    </a>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
        <a href="{{route('mixing')}}">
        <div class="card">
            <div class="card-body">
                <h5 class="text-muted">Data Mixing</h5>
                <div class="metric-value d-inline-block">
                    <h1 class="mb-1">{{$mixing}}</h1>
                </div>
                <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                </div>
            </div>
            <div id="sparkline-revenue2"></div>
        </div>
    </div>
    </a>
</div>
@endsection