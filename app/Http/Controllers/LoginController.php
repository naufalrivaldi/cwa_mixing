<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function postLogin(Request $req){
        $user = auth::attempt($req->only('username', 'password'));
        if ($user) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login')->with('error','username atau password salah');
        }        
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
