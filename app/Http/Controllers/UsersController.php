<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Store;
use Hash;
use Auth;
class UsersController extends Controller
{
    public function index() {
        $data['users'] = Users::all();
        $data['no'] = 1;
        return view('users.index', $data);
    }
    
    public function form() {
        $data['stores'] = Store::orderBy('initial', 'asc')->get();
        return view('users.form', $data);
    }

    public function val($req){
        $msg = [
            'required' => 'Kolom ini tidak boleh kosong!'
        ];

        $this->validate($req, [
            'name' => 'required',
            'username' => 'required',
            'storeId' => 'required'
        ]);
    }

    public function add(Request $req){
        $this->val($req);
        Users::create([
            'name'=>$req->name,
            'username'=>$req->username,
            'password'=>bcrypt($req->password),
            'storeId'=>$req->storeId,
            'roles'=>2
        ]);
        return redirect()->route('users')->with('success', 'Data berhasil ditambah!');
    }

    public function edit(){
        $id = $_GET['id'];
        $data['stores'] = Store::orderBy('initial', 'asc')->get();
        $data['user'] = Users::find($id);
        return view('users.form', $data);
    }

    public function update(Request $req){
        $this->val($req);
        $data = Users::find($req->id);
        $data->name=$req->name;
        $data->username=$req->username;
        if (!empty($req->password)) {            
            $data->password=bcrypt($req->password);
        } else {            
            $data->password=$req->passwordOld;
        }
        $data->storeId=$req->storeId;
        $data->save();

        return redirect()->route('users')->with('success', 'Data berhasil diubah!');
    }

    public function delete(Request $req){
        $data = Users::find($req-id);
        $data->delete();
    }
    
    public function valPwd($req){
        $msg = [
            'required' => 'Password tidak boleh kosong!',
            'same' => 'Password Baru & Konfirmasi harus sama!'
        ];

        $this->validate($req, [
            'oldpassword' => 'required',
            'newpassword' => 'required',
            'newpassword2' => 'required|same:newpassword'
        ], $msg);
    }

    public function changePwd(Request $req){
        $this->valPwd($req);
        $data = Users::find(auth()->user()->id);

        if (Hash::check($req->oldpassword, $data->password)) {
            $data->password = bcrypt($req->newpassword);
            $data->save();
            Auth::logout();
            return redirect('/')->with('success', 'Password berhasil dirubah! Silahkan login kembali.');
        }
        
        return redirect('/dashboard')->with('error', 'Password lama salah!');

    }
}
