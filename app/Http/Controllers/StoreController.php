<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;

class StoreController extends Controller
{
    public function index(){
        $data['stores'] = Store::all();
        $data['no'] = 1;
        return view('store.index', $data);
    }

    public function form(){
        return view('store.form');
    }

    public function add(Request $req){
        $this->val($req);
        Store::create([
            'name'=>$req->name,
            'initial'=>$req->initial
        ]);

        return redirect()->route('store')->with('success','Data berhasil diinput!');
    }

    public function edit(){
        $id = $_GET['id'];
        $data['store'] = Store::find($id);
        return view('store.form', $data);
    }

    public function update(Request $req){
        $this->val($req);
        $data = Store::find($req->id);
        $data->name=$req->name;
        $data->initial=$req->initial;
        $data->save();

        return redirect()->route('store')->with('success','Data berhasil diupdate!');
    }

    public function delete(Request $req){
        $data = Store::find($req->id);
        $data->delete();
    }

    public function val($req){
        $msg = [
            'required' => 'Kolom ini tidak boleh kosong!'
        ];

        $this->validate($req, [
            'name' => 'required',
            'initial' => 'required'
        ], $msg);

    }
}
