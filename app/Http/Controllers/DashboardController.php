<?php

namespace App\Http\Controllers;
use App\Mixing;
use App\Customers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        if (auth()->user()->roles == 1) {
            $data['mixing'] = Mixing::all()->count();
        } else {
            $data['mixing'] = Mixing::whereHas('users', function($query){
                $query->where('storeId', auth()->user()->storeId);
            })->count();
        }
        $data['customers'] = Customers::all()->count();
        return view('dashboard', $data);
    }
}
