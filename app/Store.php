<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'store';
    protected $fillable = ['name','initial'];
    public $timestamps = false;

    // fk
    public function users(){
        return $this->hasMany('App\Users', 'storeId');
    }
}
