<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $fillable = ['name','username','password','roles','storeId'];

    // fk
    public function store(){
        return $this->belongsTo('App\Store', 'storeId');
    }

    public function mixing(){
        return $this->hasMany('App\Mixing', 'userId');
    }
}
