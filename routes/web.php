<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@postLogin')->name('login.post');

});

Route::group(['middleware' => ['auth']], function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('/changepassword', 'UsersController@changePwd')->name('changePassword');

    Route::get('/customers', 'CustomersController@index')->name('customers');
    Route::get('/customers/form', 'CustomersController@form')->name('customers.form');
    Route::post('/customers/add', 'CustomersController@add')->name('customers.add');
    Route::get('/customers/edit', 'CustomersController@edit')->name('customers.edit');
    Route::get('/customers/{id}/view', 'CustomersController@view')->name('customers.view');
    Route::post('/customers/update', 'CustomersController@update')->name('customers.update');
    Route::post('/customers/delete', 'CustomersController@delete')->name('customers.delete');

    Route::get('/mixing', 'MixingController@index')->name('mixing');
    Route::get('/mixing/form', 'MixingController@form')->name('mixing.form');
    Route::get('/mixing/fill', 'MixingController@fill')->name('mixing.fill');
    Route::get('/mixing/showProduct', 'MixingController@showProduct')->name('mixing.showProduct');
    Route::get('/mixing/showFormula', 'MixingController@showFormula')->name('mixing.showFormula');
    Route::post('/mixing/add', 'MixingController@add')->name('mixing.add');
    Route::post('/mixing/delete', 'MixingController@delete')->name('mixing.delete');
    Route::get('/mixing/view', 'MixingController@view')->name('mixing.view');
    Route::get('/mixing/{id}/reorder', 'MixingController@reorder')->name('mixing.reorder');

    Route::get('/logout', 'LoginController@logout')->name('logout');

    Route::group(['middleware' => ['auth', 'checkRole:1']], function(){        
        Route::get('/store', 'StoreController@index')->name('store');
        Route::get('/store/form', 'StoreController@form')->name('store.form');
        Route::post('/store/add', 'StoreController@add')->name('store.add');
        Route::get('/store/edit', 'StoreController@edit')->name('store.edit');
        Route::post('/store/update', 'StoreController@update')->name('store.update');
        Route::post('/store/delete', 'StoreController@delete')->name('store.delete');

        Route::get('/merk', 'MerkController@index')->name('merk');
        Route::get('/merk/form', 'MerkController@form')->name('merk.form');
        Route::post('/merk/add', 'MerkController@add')->name('merk.add');
        Route::get('/merk/edit', 'MerkController@edit')->name('merk.edit');
        Route::post('/merk/update', 'MerkController@update')->name('merk.update');
        Route::post('/merk/delete', 'MerkController@delete')->name('merk.delete');

        Route::get('/product', 'ProductController@index')->name('product');
        Route::get('/product/form', 'ProductController@form')->name('product.form');
        Route::post('/product/add', 'ProductController@add')->name('product.add');
        Route::get('/product/edit', 'ProductController@edit')->name('product.edit');
        Route::post('/product/update', 'ProductController@update')->name('product.update');
        Route::post('/product/delete', 'ProductController@delete')->name('product.delete');

        Route::get('/users', 'UsersController@index')->name('users');
        Route::get('/users/form', 'UsersController@form')->name('users.form');
        Route::post('/users/add', 'UsersController@add')->name('users.add');
        Route::get('/users/edit', 'UsersController@edit')->name('users.edit');
        Route::post('/users/update', 'UsersController@update')->name('users.update');
        Route::post('/users/delete', 'UsersController@delete')->name('users.delete');

        Route::group(['prefix' => 'formula'], function(){
            Route::get('/', 'FormulaController@index')->name('formula');
            Route::get('/form', 'FormulaController@form')->name('formula.form');
            Route::get('/{merkId}/form', 'FormulaController@formByMerk')->name('formula.formbymerk');
            Route::get('/{id}/detail', 'FormulaController@detail')->name('formula.detail');
            Route::get('/edit', 'FormulaController@edit')->name('formula.edit');
            Route::post('/add', 'FormulaController@add')->name('formula.add');
            Route::put('/update', 'FormulaController@update')->name('formula.update');
            Route::post('/delete', 'FormulaController@delete')->name('formula.delete');
        });
    });
});


